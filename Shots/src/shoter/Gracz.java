package shoter;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
public class Gracz {
	
	private int x;
	private int y;
	private int r;
	
	private int dx;
	private int dy;
	private int speed;
	
	private boolean up;
	private boolean down;
	private boolean left;
	private boolean right;
	
	private int health;
	private boolean fire;
	private long fireTime;
	private long fireDelay;
	
	private Color color1;
	
	public Gracz() {
		x=AnimPanel.WIDTH/2;
		y=AnimPanel.HEIGHT/2;
		r=15;
		dx=0;
		dy=0;
		speed=5;
		health=10;
		fire=false;
		fireTime=System.nanoTime();
		fireDelay=200;
		color1=Color.WHITE;			//mozesz zmienic kolory
	}
	public void setUp(boolean kierunek) {
		up=kierunek;
	}
	public void setDown(boolean kierunek) {
		down=kierunek;
	}
	public void setLeft(boolean kierunek) {
		left=kierunek;
	}
	public void setRight(boolean kierunek) {
		right=kierunek;
	}
	public void setFire(boolean ogien)
	{
		fire=ogien;
	}
	
	public void update() {
		if(left) {
			dx=-speed;
		}
		if(right) {
			dx=speed;
		}
		if(up) {
			dy=-speed;
		}
		if(down) {
			dy=speed;
		}
		x=x+dx;
		y=y+dy;
		
		if(x<r)x=r;
		if(y<r)y=r;
		if(x>AnimPanel.WIDTH-r)x=AnimPanel.WIDTH-r;	
		if(y<(AnimPanel.HEIGHT/2)-r)y=(AnimPanel.HEIGHT/2)-r; //to zmienic w drugim kliencie
		if(y>(AnimPanel.HEIGHT-r))y=(AnimPanel.HEIGHT)-r;		//to tez
		
		dx=0;
		dy=0;
		if(fire) {
			long elapsed=(System.nanoTime()-fireTime)/2000000;
			if(elapsed>fireDelay) {
				AnimPanel.bullets.add(new Bullet(x,y,270));
				fireTime=System.nanoTime();
			}
		}
			
	}
	public void draw(Graphics2D g) {
		g.setColor(color1);
		g.fillOval(x-r,y-r,2*r,2*r);
		
		g.setStroke(new BasicStroke(3));
		g.setColor(color1.darker());
		g.drawOval(x-r, y-r, 2*r, 2*r);
		g.setStroke(new BasicStroke(1));
	}
}
