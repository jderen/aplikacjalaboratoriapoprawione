package shoter;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JPanel;


public class AnimPanel extends JPanel implements Runnable,KeyListener {
	
/**
	 * 
	 */
private static final long serialVersionUID = 1L;
public static int  WIDTH=1000;
public static int HEIGHT=500;
private Thread thread;
private boolean isRunning;
private BufferedImage image;
private Graphics2D g;
private int FPS=30;
public static Gracz gracz;
public static ArrayList<Bullet>bullets;

	public AnimPanel() {
		super();
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setFocusable(true);
		requestFocus();
		
	}

	public void addNotify() {
		super.addNotify();
		if(thread==null) {
			thread=new Thread(this);
			thread.start();
		}
		addKeyListener(this);
	}
	public void run() {
		
		isRunning=true;
		
		image=new BufferedImage(WIDTH,HEIGHT, BufferedImage.TYPE_INT_RGB);
		g=(Graphics2D)image.getGraphics();
		long startTime;
		long URDTimeMillis;
		long waitTime;
		long targetTime=1000/FPS;
		gracz=new Gracz();
		bullets=new ArrayList<Bullet>();
		
		while(isRunning) {
			
			startTime=System.nanoTime();
			
			update();
			render();
			draw();
			
			URDTimeMillis=(System.nanoTime()-startTime)/1000000;
			waitTime=targetTime-URDTimeMillis;
			try {
				Thread.sleep(waitTime);
			}
			catch(Exception e){	
			}

		}
	}
	public void update() {
		gracz.update();
		for(int i=0;i<bullets.size();i++) {
			boolean usun=bullets.get(i).update();
			if(usun) {
				bullets.remove(i);
				i--;
			}
		}
	}
	public void render() {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.drawString("ilosc pociskow"+bullets.size(), 10, 20);
		gracz.draw(g);
		for(int i=0;i<bullets.size();i++) {
			bullets.get(i).draw(g);
		}
	}
	public void draw() {
		Graphics g2=this.getGraphics();
		g2.drawImage(image,0,0,null);
		g2.dispose();
	}
	public void keyTyped(KeyEvent key) {
}
	public void keyPressed(KeyEvent key) {
		int code=key.getKeyCode();
		if(code==KeyEvent.VK_UP)gracz.setUp(true);
		if(code==KeyEvent.VK_DOWN)gracz.setDown(true);
		if(code==KeyEvent.VK_LEFT)gracz.setLeft(true);
		if(code==KeyEvent.VK_RIGHT)gracz.setRight(true);
		if(code==KeyEvent.VK_SPACE)gracz.setFire(true);
	}
	public void keyReleased(KeyEvent key) {
		int code=key.getKeyCode();
		if(code==KeyEvent.VK_UP)gracz.setUp(false);
		if(code==KeyEvent.VK_DOWN)gracz.setDown(false);
		if(code==KeyEvent.VK_LEFT)gracz.setLeft(false);
		if(code==KeyEvent.VK_RIGHT)gracz.setRight(false);
		if(code==KeyEvent.VK_SPACE)gracz.setFire(false);
	}
}
